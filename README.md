Die Architecture Center Ltd bietet Ihnen TOGAF® 9 Foundation und Certified (Stufe 1 und 2), ArchiMate® 3.0 sowie IT4IT™ Trainings mit anschliessender Zertifizierung an.
Unsere Mitarbeiter sind erfahrene Consultants und Trainer. Die Architecture Center Ltd ist ein Mitglied bei The Open Group®. ||

Address: Eichborndamm 167 Gb-55, 13403 Berlin, Germany ||
Phone: +49 30 30807165
